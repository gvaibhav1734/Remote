package vs.com.remote;

import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ConstraintLayout root;
    private ViewPager viewPager;
    private String url = "http://192.168.0.125:254/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        viewPager = findViewById(R.id.main_vp);
        root = findViewById(R.id.cl_root);
        viewPager.setAdapter(new CustomPagerAdapter());
//        sharedPreferences = getPreferences(MODE_PRIVATE);
//        url = sharedPreferences.getString("url", "");
//        if (url.equals(""))
//            saveUrl();
    }

    class CustomPagerAdapter extends PagerAdapter {
        public static final int VIDEOCON = 0;
        public static final int LG_TV = 1;
        public static final int BLU_RAY = 2;
        private int layouts[] = {R.layout.layout_videocon, R.layout.layout_lg_tv, R.layout.layout_blu_ray};
        private String titles[] = {"Videocon", "LG TV", "Blu-Ray"};


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = getLayoutInflater().inflate(layouts[position], container, false);
            container.addView(view);
            if (position == VIDEOCON)
                videocon(view);
            else if (position == LG_TV)
                lgtv(view);
            else if (position == BLU_RAY)
                bluray(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }

    public void videocon(final View view) {
        final String identifier = "VIDEOCON_";
        int numpadButtons[] = {R.id.numpad_0, R.id.numpad_1, R.id.numpad_2, R.id.numpad_3
                , R.id.numpad_4, R.id.numpad_5, R.id.numpad_6, R.id.numpad_7
                , R.id.numpad_8, R.id.numpad_9};
        for (int numpadButton : numpadButtons) {
            final Button button = view.findViewById(numpadButton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendIr(identifier + button.getText().toString());
                }
            });
        }
        view.findViewById(R.id.btn_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //saveUrl();
            }
        });
        view.findViewById(R.id.btn_power).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "POWER");
            }
        });
        view.findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "HOME");
            }
        });
        view.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "OK");
            }
        });
        view.findViewById(R.id.btn_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "INFO");
            }
        });
        view.findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "MENU");
            }
        });
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "BACK");
            }
        });
        view.findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "EXIT");
            }
        });
        view.findViewById(R.id.btn_mute).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "MUTE");
            }
        });
        view.findViewById(R.id.btn_channel_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "CHANNEL_UP");
            }
        });
        view.findViewById(R.id.btn_channel_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "CHANNEL_DOWN");
            }
        });
        view.findViewById(R.id.btn_volume_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "VOLUME_UP");
            }
        });
        view.findViewById(R.id.btn_volume_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "VOLUME_DOWN");
            }
        });
        view.findViewById(R.id.btn_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "UP");
            }
        });
        view.findViewById(R.id.btn_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "RIGHT");
            }
        });
        view.findViewById(R.id.btn_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "DOWN");
            }
        });
        view.findViewById(R.id.btn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "LEFT");
            }
        });
        view.findViewById(R.id.btn_arrowpad_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "BACK");
            }
        });
        view.findViewById(R.id.btn_arrowpad_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "EXIT");
            }
        });
        view.findViewById(R.id.btn_relay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "RELAY");
            }
        });
        view.findViewById(R.id.btn_flip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.findViewById(R.id.cl_arrowpad).getVisibility() == View.INVISIBLE) {
                    // Set arrowpad visible
                    view.findViewById(R.id.cl_arrowpad).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.cl_numpad).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_ok).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_arrowpad_back).setVisibility(View.VISIBLE);

                    view.findViewById(R.id.btn_arrowpad_exit).setVisibility(View.VISIBLE);
                } else {
                    // Set numpad visible
                    view.findViewById(R.id.cl_numpad).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.cl_arrowpad).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_ok).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.btn_arrowpad_back).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_arrowpad_exit).setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void lgtv(final View view) {
        final String identifier = "LG_TV_";
        int numpadButtons[] = {R.id.numpad_0, R.id.numpad_1, R.id.numpad_2, R.id.numpad_3
                , R.id.numpad_4, R.id.numpad_5, R.id.numpad_6, R.id.numpad_7
                , R.id.numpad_8, R.id.numpad_9};
        for (int numpadButton : numpadButtons) {
            final Button button = view.findViewById(numpadButton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendIr(identifier + button.getText().toString());
                }
            });
        }
        view.findViewById(R.id.btn_power).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "POWER");
            }
        });
        view.findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "HOME");
            }
        });
        view.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "OK");
            }
        });
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "BACK");
            }
        });
        view.findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "EXIT");
            }
        });
        view.findViewById(R.id.btn_mute).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "MUTE");
            }
        });
        view.findViewById(R.id.btn_channel_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "CHANNEL_UP");
            }
        });
        view.findViewById(R.id.btn_channel_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "CHANNEL_DOWN");
            }
        });
        view.findViewById(R.id.btn_volume_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "VOLUME_UP");
            }
        });
        view.findViewById(R.id.btn_volume_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "VOLUME_DOWN");
            }
        });
        view.findViewById(R.id.btn_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "UP");
            }
        });
        view.findViewById(R.id.btn_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "RIGHT");
            }
        });
        view.findViewById(R.id.btn_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "DOWN");
            }
        });
        view.findViewById(R.id.btn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "LEFT");
            }
        });
        view.findViewById(R.id.btn_arrowpad_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "BACK");
            }
        });
        view.findViewById(R.id.btn_arrowpad_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "EXIT");
            }
        });
        view.findViewById(R.id.btn_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "SETTINGS");
            }
        });
        view.findViewById(R.id.btn_aspect_ratio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "ASPECT_RATIO");
            }
        });
        view.findViewById(R.id.btn_input).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "INPUT");
            }
        });
        view.findViewById(R.id.btn_3d).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "3D");
            }
        });
        view.findViewById(R.id.btn_flip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.findViewById(R.id.cl_arrowpad).getVisibility() == View.INVISIBLE) {
                    // Set arrowpad visible
                    view.findViewById(R.id.cl_arrowpad).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.cl_numpad).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_ok).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_arrowpad_back).setVisibility(View.VISIBLE);

                    view.findViewById(R.id.btn_arrowpad_exit).setVisibility(View.VISIBLE);
                } else {
                    // Set numpad visible
                    view.findViewById(R.id.cl_numpad).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.cl_arrowpad).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_ok).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.btn_arrowpad_back).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.btn_arrowpad_exit).setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void bluray(View view) {
        final String identifier = "BLU_RAY_";
        view.findViewById(R.id.btn_power).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "POWER");
            }
        });
        view.findViewById(R.id.btn_eject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "EJECT");
            }
        });
        view.findViewById(R.id.btn_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "PLAY");
            }
        });
        view.findViewById(R.id.btn_pause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "PAUSE");
            }
        });
        view.findViewById(R.id.btn_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "STOP");
            }
        });
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "BACK");
            }
        });
        view.findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "HOME");
            }
        });
        view.findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "MENU");
            }
        });
        view.findViewById(R.id.btn_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "UP");
            }
        });
        view.findViewById(R.id.btn_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "RIGHT");
            }
        });
        view.findViewById(R.id.btn_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "DOWN");
            }
        });
        view.findViewById(R.id.btn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "LEFT");
            }
        });
        view.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIr(identifier + "OK");
            }
        });
    }

    public void sendIr(final String key) {
//        if (url.equals("")) {
//            saveUrl();
//            Snackbar.make(root, "Click button again", Snackbar.LENGTH_SHORT);
//            return;
//        }
        final int resourceKey = getResources().getIdentifier(key, "string"
                , getApplicationContext().getPackageName());
        if (resourceKey != 0) {
            StringRequest sendIrRequest = new StringRequest(Request.Method.POST
                    , url
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Volley Success : " + response);
                }
            }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Volley Failure : " + error.getMessage());
                }
            }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("key", getValue(resourceKey));
                    //Log.d("key",headers.get("key"));
                    return headers;
                }
            };
            VolleyHelper.getInstance(this).addToRequestQueue(sendIrRequest);
        } else {
            Snackbar.make(root, "Key not mapped", Snackbar.LENGTH_SHORT);
        }
    }

    public String getValue(int key) {
        return "<start>"
                + getString(key)
                + "<end>|";
    }

//    public void saveUrl() {
//        WifiManager wifiManager =
//                (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//        if (!wifiManager.isWifiEnabled()) {
//            Snackbar.make(root, "Connect to Wifi", Snackbar.LENGTH_SHORT).show();
//        } else {
//            String SSID = wifiManager.getConnectionInfo().getSSID();
//            if (SSID != null && (SSID.equals("\"tenda\"") || SSID.equals("\"Tenda_5G\""))) {
//                StringRequest stringRequest = new StringRequest(Request.Method.GET
//                        , "http://whatismyip.akamai.com/"
//                        , new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d(TAG, "Request for ip address successful ");
//                        url = "http://" + response + ":254/";
//                        Snackbar.make(root, "Reset successful", Snackbar.LENGTH_SHORT);
//                        sharedPreferences.edit().putString("url", url).apply();
//                    }
//                }
//                        , new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d(TAG, "Request for ip address failed");
//                    }
//                });
//                VolleyHelper.getInstance(this).addToRequestQueue(stringRequest);
//            } else {
//                Snackbar.make(root, "Connect to Tenda", Snackbar.LENGTH_SHORT).show();
//            }
//        }
//    }
}
