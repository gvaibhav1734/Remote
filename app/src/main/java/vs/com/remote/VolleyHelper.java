package vs.com.remote;

import android.content.Context;
import android.os.Build;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BaseHttpStack;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

public class VolleyHelper {
    private static VolleyHelper mInstance;
    RequestQueue requestQueue;
    private VolleyHelper(Context context){
        final int MAX_SERIAL_THREAD_POOL_SIZE = 1;
        final int MAX_CACHE_SIZE = 2 * 1024 * 1024; //2 MB

        Cache cache = new DiskBasedCache(context.getCacheDir(), MAX_CACHE_SIZE);
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache,network,MAX_SERIAL_THREAD_POOL_SIZE);
        requestQueue = Volley.newRequestQueue(context);
    }
    public static synchronized VolleyHelper getInstance(Context context) {
        if(mInstance==null) {
            mInstance = new VolleyHelper(context);
            return mInstance;
        }
        return mInstance;
    }
    public <T> void addToRequestQueue(Request<T> request) {
        requestQueue.add(request);
    }
}
