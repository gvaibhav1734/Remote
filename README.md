# Remote
Android app to simulate LG TV Remote, Videocon Remote and Blu-ray remote.

## How it works
* Using an IR receiver raw data emitted from each button (on a physical remote)
is obtained and stored in signals.xml file.
* On clicking the button (in app) respective raw data sent to a Wifi Server(ESP8266) 
using Volley library.
* Raw Data received is emitted by arduino using an IR transmitter.

